package ru.t1.ytarasov.tm.api;

import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

}
