package ru.t1.ytarasov.tm.api;

public interface ITaskController {

    void createTask();

    void clearTask();

    void showTasks();

}
