package ru.t1.ytarasov.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
