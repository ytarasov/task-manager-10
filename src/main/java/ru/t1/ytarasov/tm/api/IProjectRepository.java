package ru.t1.ytarasov.tm.api;

import ru.t1.ytarasov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

}
