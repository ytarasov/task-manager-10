package ru.t1.ytarasov.tm.api;

import ru.t1.ytarasov.tm.model.Command;

public interface ICommandRepository {
    Command[] getTerminalCommands();
}
