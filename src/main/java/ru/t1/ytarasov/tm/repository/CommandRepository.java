package ru.t1.ytarasov.tm.repository;

import ru.t1.ytarasov.tm.api.ICommandRepository;
import ru.t1.ytarasov.tm.constant.ArgumentConst;
import ru.t1.ytarasov.tm.constant.TerminalConst;
import ru.t1.ytarasov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );
    private final static Command EXIT = new Command(
            TerminalConst.EXIT, null, "Exit from application."
    );
    private final static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "Display system info."
    );
    private final static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );
    private final static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display program version."
    );
    private final static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Show project list"
    );
    private final static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project"
    );
    private final static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Delete all projects"
    );
    private final static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Show all tasks"
    );
    private final static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task"
    );
    private final static Command TASK_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Delete all tasks"
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, EXIT, HELP, INFO, PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR, TASK_LIST, TASK_CREATE, TASK_CLEAR,
            VERSION
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
